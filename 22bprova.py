import sys,socket
from subprocess import Popen, PIPE
import argparse


parser = argparse.ArgumentParser(description=\
        """server one by one""")

parser.add argument("-p","--port", type=int,help="port al que ens conectem", default=50001, dest="port")

args=parser.parse_args()

#------------------------------------


HOST = ''
#PORT = 50001

PORT=args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
  conn, addr = s.accept()
  print("Connected by", addr)
  command = ["date"]
  pipeData = Popen(command,stdout=PIPE)
  for line in pipeData.stdout:
    conn.send(line)
  conn.close()

