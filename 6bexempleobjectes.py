# /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n nlin] [-f file]
#  10 lines, file o stdin
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
# Exemple de programació Objectes POO
# -------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de 
  /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self, userline):	
    "Constructor objectes UnixUser"
    userField=userline.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6]

  def show(self):
    "Mostrar les dades de l'usuari"
    print("login:%s uid: %d gid:%d" % (self.login, self.uid, self.gid))
  def sumaun(self):
      self.uid+=1
  def __str__(self):
      "Funció per retornar un string del objecte"
      return "%s %d %d" % (self.login, self.uid, self.gid)


print("Programa")

user1=UnixUser("pere:1000:100::/home/pere:/bin/bash")

print(user1)

exit(0)
