import sys
from subprocess import Popen, PIPE
import argparse
parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
parser.add_argument('sqlStatment', help='Sentència SQL a executar',metavar='sentènciaSQL')
args = parser.parse_args()

cmd = "psql -qtA -F',' -h 172.17.0.2 -U postgres  training"
pipeData = Popen(cmd, shell = True, bufsize=0,universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

pipeData.stdin.write(args.sqlStatment+"\n\q\n")

for line in pipeData.stdout:
  print (line)
sys.exit(0)

